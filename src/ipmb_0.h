/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmb_0.h
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management tasks for incoming and outgoing messages from IPMB-A and IPMB-B.
 *
 * This header file has tasks declarations responsible to manage the incoming and outgoing messages from IPMB-A and IPMB-B.
 * The messages received from both IPMB are discriminated as response or request then they are sent to specific queues.
 * 
 * The file also defines different queues for incoming and outgoing messages, and for incoming messages was divided into response and request.
 */


#ifndef IPMB_0_H
#define IPMB_0_H

//FreeRTOS resources
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "sdr_definitions.h"


/**
 * IPMB address used when the Hardware Address is invalid
 */
#define IPMB_ERROR_ADDR 0xEE

/**
 * IPMB message size
 */
#define IPMB_MAX_MESSAGE_SIZE 32

typedef struct {
	char channel;
	int length;
	uint8_t data[IPMB_MAX_MESSAGE_SIZE];
} data_ipmb;


/**
 * @brief IPMB address is stored in global for faster access. 8bit format.
 * 
 * This variable is written here during the ipmb_0_msg_sender_task()
 * initialization
 */
extern uint8_t ipmb_0_addr;


/**
 * @brief Activate/deactivate IPMB-0 debugging prints
 *
 * This flag can be set or reset by the user
 */
extern _Bool ipmb_0_debug_flag;


/**
 * @name IPMB-0 Queue
 * 
 * @brief The following queues are used to send and receive IPMI messages 
 * through the IPMB-0. They are the interface to this bus.
 * @{
 */
/**
 * @brief Incoming Requests
 * 
 * Incoming Requests coming from IPMB-0 are available in
 * 'queue_ipmb0_req_in'.
 * 
 */
extern QueueHandle_t queue_ipmb0_req_in;
/**
 * @brief Incoming Responses
 * 
 * Incoming Responses coming from IPMB-0 are available in 
 * 'queue_ipmb0_res_in'.
 * 
 */
extern QueueHandle_t queue_ipmb0_res_in;
/**
 * @brief Outgoing Messages
 * 
 * Outgoing messages generated in the IPMC must be put into 
 * 'queue_ipmb0_out' (Requests or responses)
 * 
 */
extern QueueHandle_t queue_ipmb0_out;
///@}


/**
 * @brief IPMB-0 Sensor reading callback
 *
 * This function generates response to the Get Sensor Reading command when it
 * refers to the IPMB-0 Sensor.
 *
 */
sensor_reading_status_t ipmb0_sensor_reading(sensor_reading_t* sensor_reading);



#endif
