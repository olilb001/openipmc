/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_storage.c
 * 
 * @authors Bruno Augusto Casu
 * @authors Andre Muller Cascadan
 * 
 * @brief  Response functions for Storage Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Storage Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 * 
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().
 */

#include <string.h>
#include <stdint.h>

#include "fru_inventory_manager.h"
#include "ipmc_ios.h"

/**
 * @{
 * @name Storage NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get FRU Area Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_fru_inventory_area_info (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

    if (req_len != 1)
    {
        //ipmc_ios_printf("Get FRU Inventory Area Info: Request data length invalid.\r\n");
        *completion_code = 0xC7; // Request data length invalid.
        *res_len         = 0;
        return;
    }
    
    uint8_t const fru_id = request_bytes[0];
    
    int fru_inventory_size = get_fru_inventory_size( fru_id );
    
    if ( fru_inventory_size < 0 ) 
    {
        //ipmc_ios_printf("Read FRU Data: Parameter out of range.\r\n");
        *completion_code = 0xC9; // Parameter out of range.
        *res_len         = 0;
        return;
    }
    
    response_bytes[0] = (uint8_t) ((fru_inventory_size & 0x00ff) >> 0); // size of FRU Inventory size ls byte
    response_bytes[1] = (uint8_t) ((fru_inventory_size & 0xff00) >> 8); // size of FRU Inventory size ms byte
    response_bytes[2] = 0x00; // 00h = device is accessed by bytes
    
    *res_len = 3;
    *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Read FRU Data" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 * This function is part of the Inventory management. As a Read FRU Data command is received this function calls get_inventory_data()
 * to access inventory and retrieve the requested data.
 */
void ipmi_cmd_read_fru_data( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    const int RECORD_DATA_BUFF_SIZE = 23;
    
    
    if (req_len != 4)
    {
        //ipmc_ios_printf("Read FRU Data: Request data length invalid.\r\n");
        *completion_code = 0xC7; // Request data length invalid.
        *res_len         = 0;
        return;
    }
    
    uint8_t      const fru_id    = request_bytes[0]; // FRU Device ID
    uint8_t      const offset_ms = request_bytes[2];
    uint8_t      const offset_ls = request_bytes[1];
    uint16_t     const offset = (( (uint16_t)offset_ms ) << 8) + (( (uint16_t)offset_ls ) << 0);
    
    size_t  const bytes_to_read = request_bytes[3];
    
    if (bytes_to_read > RECORD_DATA_BUFF_SIZE)
    {
        //ipmc_ios_printf("Read FRU Data: Cannot return number of requested data bytes.\r\n");
        *completion_code = 0xCA; // Cannot return number of requested data bytes.
        *res_len         = 0;
        return;
    }
    
    uint8_t record_data_buff[RECORD_DATA_BUFF_SIZE];
    
    // Get a segment of FRU Inventory
    int number_of_read_bytes = get_fru_inventory_data( fru_id, offset, bytes_to_read, record_data_buff);
    
    if( number_of_read_bytes < 0)
    {
    	//ipmc_ios_printf("Read FRU Data: Parameter out of range.\r\n");
        *completion_code = 0xC9; // Parameter out of range.
        *res_len         = 0;
        return;
    }

    for (int i = 0; i < number_of_read_bytes; i++)
    	response_bytes[i+1] = record_data_buff[i]; // Load response bytes from buffer
    
    response_bytes[0] = number_of_read_bytes;
    *res_len = number_of_read_bytes + 1;
    *completion_code = 0; // OK
    
}


/**
 * @brief Specific function to execute for "Write FRU Data" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 * Implements the "Write FRU Data" Command. OpenIPMC does not currently allow write operations in FRU Info, so all the inventory is
 * considered *protected*. This command always return *Completion Code 80h* (write-protected offset).
 */
void ipmi_cmd_write_fru_data( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{


    if (req_len < 3)
    {
        //ipmc_ios_printf("Write FRU Data: Request data length invalid.\r\n");
        *completion_code = 0xC7; // Request data length invalid.
        *res_len         = 0;
        return;
    }

    if (request_bytes[0] != 0) // FRU Device ID must be 0
    {
    	//ipmc_ios_printf("Read FRU Data: Parameter out of range.\r\n");
        *completion_code = 0xC9; // Parameter out of range.
        *res_len         = 0;
        return;
    }

    // The FRU Info is protected in current OpenIPMC implementation
    response_bytes[0] = 0;
    *res_len = 1;
    *completion_code = 0x80;  // Write-protected offset

}


/**
 * @brief Get information about the SEL repository
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 * This function is currently retrieving a standard response for an empty SEL repository. SEL is still not implemented.
 */
void ipmi_cmd_get_sel_info (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

    response_bytes[0] = 0x51; // SEL Version for IPMI 1.5
    response_bytes[1] = 0;    // Inform empty SEL repository
    response_bytes[2] = 0;
    response_bytes[3] = 0;    // Inform no free space
    response_bytes[4] = 0;

    response_bytes[5] = 0xFF; // Timestamp for empty repository
    response_bytes[6] = 0xFF;
    response_bytes[7] = 0xFF;
    response_bytes[8] = 0xFF;

    response_bytes[9]  = 0xFF; // Timestamp for empty repository
    response_bytes[10] = 0xFF;
    response_bytes[11] = 0xFF;
    response_bytes[12] = 0xFF;

    response_bytes[13] = 0;    //Operation Support bits all Zero

    *res_len = 14;
    *completion_code = 0; //Ok
}

///@}



