/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_msg_command_switch.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 *
 * @brief  Command switch to direct IMPI requests to the specific response function.
 *
 * This file directs IPMI requests sent by the Shelf Manager based on the Network Function and Command (definition in IPMI v1.5 and PICMG v3.0).
 * As the command is identified, the request is directed to a response function that returns the response bytes to be sent through IPMB.
 */

#include <stdint.h>
#include "ipmi_msg_manager.h"
#include "ipmc_ios.h"

static char* ipmi_req_name = "IPMI Req Name:";
#define DEBUG_PRINT( X ) if( ipmi_msg_debug_flag ) ipmc_ios_printf("%s %s\r\n", ipmi_req_name, X );

/*
 * This functions generates the response for each command.
 * They are defined in files dedicated for each Network Function
 */
void ipmi_cmd_get_device_id                   (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_picmg_prop                  (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_event_rec                   (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_loc_record_id        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_activation              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_reserve_device_sdr_repos        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sensor_threshold            (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_sdr                  (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_device_sdr_info             (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_compute_power_prop              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_power_level                 (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_power_level                 (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_inventory_area_info     (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_read_fru_data                   (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_write_fru_data                  (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sel_info                    (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_port_state                  (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_address_info                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_port_state                  (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sensor_reading              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_sensor_event_enable         (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_sensor_event_status         (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_shelf_address_info          (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_shelf_address_info          (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_fru_control                     (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_fru_control_capabilities        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_target_upgrade_capabilities (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_component_properties        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_abort_firmware_upgrade          (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_initiate_upgrade_action         (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_upload_firmware_block           (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_finish_firmware_upload          (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_upgrade_status              (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_activate_firmware               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_query_selftest_results          (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_query_rollback_status           (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_initiate_manual_rollback        (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_led_prop                (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_led_color_capab             (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_led_state               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_led_state               (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_set_fru_activation_policy       (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);
void ipmi_cmd_get_fru_activation_policy       (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);

static void ipmi_cmd_invalid_command        (uint8_t  netfn, uint8_t  command, int *res_len, uint8_t *completion_code);

int ipmi_msg_solve_request_ipmb0(uint8_t  netfn,
                                 uint8_t  command,
                                 uint8_t* request_bytes,
                                 int      req_len,
                                 uint8_t* completion_code,
                                 uint8_t* response_bytes,
                                 int*     res_len)
{
    switch(netfn)
    {
    //  case 0x00: // Chassis
    //
    //  case 0x02: // Bridge
    //
        case 0x04: // Sensor/Event
            switch(command)
            {
                case 0x00: // Set Event Receiver
                	DEBUG_PRINT("Set Event Receiver");
                    ipmi_cmd_set_event_rec(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x20: // Get Device SDR Info
                	DEBUG_PRINT("Get Device SDR Info");
                    ipmi_cmd_get_device_sdr_info(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x21: // Get Device SDR
                	DEBUG_PRINT("Get Device SDR");
                    ipmi_cmd_get_device_sdr(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x22: // Reserve Device SDR Repository
                    DEBUG_PRINT("Reserve Device SDR Repository");
                    ipmi_cmd_reserve_device_sdr_repos(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x27: // Get Sensor Threshold
                    DEBUG_PRINT("Get Sensor Threshold");
                    ipmi_cmd_get_sensor_threshold(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x28: // Set Sensor Event Enable
                    DEBUG_PRINT("Set Sensor Event Enable");
                    ipmi_cmd_set_sensor_event_enable(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2B: // Get Sensor Event Status
                    DEBUG_PRINT("Get Sensor Event Status");
                    ipmi_cmd_get_sensor_event_status(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2D: // Get Sensor Reading
                    DEBUG_PRINT("Get Sensor Reading");
                    ipmi_cmd_get_sensor_reading(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                default:
                    ipmi_cmd_invalid_command(netfn, command, res_len, completion_code);

            }
        break;

        case 0x06: // App
            switch(command)
            {
                case 0x01: // Get Device ID
                	DEBUG_PRINT("Get Device ID");
                    ipmi_cmd_get_device_id(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

            // case 0x04: // Get Self Test Results
            //     ipmi_cmd_get_self_test_results(request_bytes, req_len, response_bytes, res_len, completion_code);
            // break;

                default:
                    ipmi_cmd_invalid_command(netfn, command, res_len, completion_code);

            }
        break;

    //	case 0x08: // Firmware
    //    
        case 0x0A: // Storage
            switch(command)
            {
                case 0x10: // Get FRU Inventory Area Info
                    DEBUG_PRINT("Get FRU Inventory Area Info");
                    ipmi_cmd_get_fru_inventory_area_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x11: // Read FRU Data
                    DEBUG_PRINT("Read FRU Data");
                    ipmi_cmd_read_fru_data (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x12: // Write FRU Data
                    DEBUG_PRINT("Write FRU Data");
                    ipmi_cmd_write_fru_data (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x40: // Read SEL Info
                    DEBUG_PRINT("Read SEL Info");
                    ipmi_cmd_get_sel_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                default:
                    ipmi_cmd_invalid_command(netfn, command, res_len, completion_code);

            }
        break;

    //  case 0x0C:  // Transport
    //  
        case 0x2C:  // PICMG
            switch(command)
            {
                case 0x00: // Get PICMG Properties
                    DEBUG_PRINT("Get PICMG Properties");
                    ipmi_cmd_get_picmg_prop(request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x01: // Get Address Info
                    DEBUG_PRINT("Get Address Info");
                    ipmi_cmd_get_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x02: // Get Shelf Address Info
                    DEBUG_PRINT("Get Shelf Address Info");
                    ipmi_cmd_get_shelf_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x03: // Set Shelf Address Info
                    DEBUG_PRINT("Set Shelf Address Info");
                    ipmi_cmd_set_shelf_address_info (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x04: // FRU Control
                    DEBUG_PRINT("FRU Control");
                    ipmi_cmd_fru_control (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x05: // Get FRU LED Properties
                    DEBUG_PRINT("Get FRU LED Properties");
                    ipmi_cmd_get_fru_led_prop (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x06: // Get LED Color Capabilities
                    DEBUG_PRINT("Get LED Color Capabilities");
                    ipmi_cmd_get_led_color_capab (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x07: // Set FRU LED State
                    DEBUG_PRINT("Set FRU LED State");
                    ipmi_cmd_set_fru_led_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x08: // Get FRU LED State
                    DEBUG_PRINT("Get FRU LED State");
                    ipmi_cmd_get_fru_led_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0A: // Set FRU Activation Policy
                    DEBUG_PRINT("Set FRU Activation Policy");
                    ipmi_cmd_set_fru_activation_policy (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0B: // Get FRU Activation Policy
                    DEBUG_PRINT("Get FRU Activation Policy");
                    ipmi_cmd_get_fru_activation_policy (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0C: // Set FRU Activation
                    DEBUG_PRINT("Set FRU Activation");
                    ipmi_cmd_set_fru_activation (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0D: // Get Device Locator Record ID
                    DEBUG_PRINT("Get Device Locator Record ID");
                    ipmi_cmd_get_device_loc_record_id (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x0E: // Set Port State (not supported)
                    DEBUG_PRINT("Set Port State");
                    ipmi_cmd_set_port_state (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x10: // Compute Power Properties
                    DEBUG_PRINT("Compute Power Properties");
                    ipmi_cmd_compute_power_prop (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x11: // Set Power Level
                    DEBUG_PRINT("Set Power Level");
                    ipmi_cmd_set_power_level (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x12: // Get Power Level
                    DEBUG_PRINT("Get Power Level");
                    ipmi_cmd_get_power_level (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x1E: // FRU Control Capabilities
                    DEBUG_PRINT("FRU Control Capabilities");
                    ipmi_cmd_fru_control_capabilities (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2E: // HPM.1: Get target upgrade capabilities
                    DEBUG_PRINT("HPM.1 Get target upgrade capabilities");
                    ipmi_cmd_get_target_upgrade_capabilities (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x2F: // HPM.1: Get component properties
                    DEBUG_PRINT("HPM.1 Get component properties");
                    ipmi_cmd_get_component_properties (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x30: // HPM.1: Abort Firmware Upgrade
                    DEBUG_PRINT("HPM.1 Abort Firmware Upgrade");
                    ipmi_cmd_abort_firmware_upgrade (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x31: // HPM.1: Initiate upgrade action
                    DEBUG_PRINT("HPM.1 Initiate upgrade action");
                    ipmi_cmd_initiate_upgrade_action (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x32: // HPM.1: Upload firmware block
                    DEBUG_PRINT("HPM.1 Upload firmware block");
                    ipmi_cmd_upload_firmware_block (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x33: // HPM.1: Finish firmware upload
                    DEBUG_PRINT("HPM.1 Finish firmware upload");
                    ipmi_cmd_finish_firmware_upload (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x34: // HPM.1: Get upgrade status
                    DEBUG_PRINT("HPM.1 Get upgrade status");
                    ipmi_cmd_get_upgrade_status (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x35: // HPM.1: Activate firmware
                    DEBUG_PRINT("HPM.1 Activate firmware");
                    ipmi_cmd_activate_firmware (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x36: // HPM.1: Query self-test results
                    DEBUG_PRINT("HPM.1 Query self-test results");
                    ipmi_cmd_query_selftest_results (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x37: // HPM.1: Query Rollback status
                    DEBUG_PRINT("HPM.1 Query Rollback status");
                    ipmi_cmd_query_rollback_status (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x38: // HPM.1: Initiate Manual Rollback
                    DEBUG_PRINT("HPM.1 Initiate Manual Rollback");
                    ipmi_cmd_initiate_manual_rollback (request_bytes, req_len, response_bytes, res_len, completion_code);
                break;

                case 0x3E: // UNKNOWN CMD 3E (not supported)
                    ipmi_cmd_invalid_command (netfn, command, res_len, completion_code);
                break;

                default:
                    ipmi_cmd_invalid_command(netfn, command, res_len, completion_code);

            }
        break;

        default:
            ipmi_cmd_invalid_command(netfn, command, res_len, completion_code);


    }
    return 1;
}

/**
 * @brief Specific function to provide a response for non-implemented commands.
 *
 * This function is called inside the Switch/Case of ipmi_msg_solve_request_ipmb0() in order to provide a 
 * proper response for unsupported commands coming from Shelf Manager.
 *
 * @param netfn           Byte which specifies the Network Function in the IPMI request.
 * @param command         Byte which specifies the Command in the IPMI request.
 * @param res_len         Number of bytes in the response returned. Always Zero in this case.
 * @param completion_code The completion code. Always 0xC1 in this case (Invalid Command).
 */
static void ipmi_cmd_invalid_command( uint8_t netfn, uint8_t command, int *res_len, uint8_t *completion_code )
{
    ipmc_ios_printf("Unknown Command Received - NetFn:0x%x Cmd:0x%x\r\n", netfn, command);
    *res_len = 0;
    *completion_code = 0xC1;     // Command Invalid (defined in IPMI v1.5) - unrecognized or unsupported command
}






