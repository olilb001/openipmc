/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file front_leds.c
 * 
 * @author Andre Muller Cascadan
 * 
 * @brief  This module contains all the resources to manage the Front Panel LEDs.
 */ 

// FreeRTOS includes
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"

#include "front_leds.h"
#include "ipmc_ios.h"

TaskHandle_t ipmc_blue_led_blink_task_handle = NULL;

/**
 * Queue for handling Blue LED behavior.
 */
QueueHandle_t queue_blue_led_mode = NULL;

void front_leds_set_blue_led_mode( int blue_led_mode )
{
    if( queue_blue_led_mode != NULL )
        xQueueSendToBack(queue_blue_led_mode, &blue_led_mode, 0UL);
}

void ipmc_blue_led_blink_task( void )
{

    int        blue_led_mode = BLUE_LED_OFF;
    TickType_t block_mode     = 0;
    
    // Waits for the peripherals to be available
    while ( ipmc_ios_ready() != pdTRUE )
        vTaskDelay( pdMS_TO_TICKS(100) );
    
    queue_blue_led_mode = xQueueCreate( 3, sizeof( int ) );
    
  
    for(;;)
    {
        
        xQueueReceive(queue_blue_led_mode, &blue_led_mode, block_mode);
        
        switch( blue_led_mode )
        {
            case BLUE_LED_SHORT_BLINK:
                
                // When blinking, task do not block
                block_mode = 0; 
                
                // Blink
                ipmc_ios_blue_led(0);
                vTaskDelay( pdMS_TO_TICKS(450) );
                
                ipmc_ios_blue_led(1);
                vTaskDelay( pdMS_TO_TICKS(100) );
                
                ipmc_ios_blue_led(0);
                vTaskDelay( pdMS_TO_TICKS(450) );
                break;
        
            case BLUE_LED_LONG_BLINK:
                
                block_mode = 0; 
                
                ipmc_ios_blue_led(0);
                vTaskDelay( pdMS_TO_TICKS(50) );
                
                ipmc_ios_blue_led(1);
                vTaskDelay( pdMS_TO_TICKS(900) );
                
                ipmc_ios_blue_led(0);
                vTaskDelay( pdMS_TO_TICKS(50) );
                break;
                
            case BLUE_LED_ON:
                
                // When not blinking, task blocks
                block_mode = portMAX_DELAY;
                
                ipmc_ios_blue_led(1);
                break;
            
            case BLUE_LED_OFF:
            default:
                
                block_mode = portMAX_DELAY;
                
                ipmc_ios_blue_led(0);
                break;
        }
    }
}
